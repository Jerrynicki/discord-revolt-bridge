import discord
import defectio
from . import revoltc
from . import msgmap

client = discord.Client()

def revolt_message_to_str(message):
    try:
        nick = message.channel.server.get_member_named(message.author.name).nickname
    except:
        nick = None

    if nick is None:
        userstr = f"**{message.author.name}**"
    else:
        userstr = f"**{member.nickname}** ({message.author.name})"

    attachments = ""
    for att in message.attachments:
        url = f"https://autumn.revolt.chat/{att.tag}/{att.id}" # workaround, att.url raises exc
        attachments += url + "\n"
    
    return f"{userstr} (revolt)\n{message.content}\n{attachments}"

@client.event
async def on_ready():
    print("discord client ready")

@client.event
async def on_message(message: discord.Message):
    print("discord message", message.content)

    if message.author.id != client.user.id:
        await revoltc.discord_message_to_revolt(message)

@client.event
async def on_message_delete(message: discord.Message):
    if message.author.id != client.user.id:
        await revoltc.discord_deleted((message.channel.id, message.id))

@client.event
async def on_message_edit(before: discord.Message, message: discord.Message):
    if message.author.id != client.user.id:
        await revoltc.discord_edited(message)

async def revolt_deleted(revolt_id):
    discord_id = msgmap.get_discord_id(revolt_id)
    if discord_id is not None:
        chan = client.get_channel(discord_id[0])
        msg = chan.get_partial_message(discord_id[1])

        await msg.delete()

        msgmap.delete_discord_id(discord_id)

    msgmap.delete_revolt_id(revolt_id)

async def revolt_edited(message):
    discord_id = msgmap.get_discord_id((message.channel.id, message.id))
    if discord_id is not None:
        chan = client.get_channel(discord_id[0])
        msg = chan.get_partial_message(discord_id[1])

        await msg.edit(content=revolt_message_to_str(message))

        msgmap.delete_discord_id(discord_id)

    msgmap.delete_revolt_id((message.channel.id, message.id))

async def revolt_message_to_discord(message: defectio.Message):
    rserver = message.server.name
    rchannel = message.channel.name

    for guild in client.guilds:
        if guild.name == rserver:
            for channel in guild.channels:
                if channel.name == rchannel:
                    discord_message = await channel.send(revolt_message_to_str(message))
                    msgmap.new_link((message.channel.id, message.id), (discord_message.channel.id, discord_message.id))
                    break
            break