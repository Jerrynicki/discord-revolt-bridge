revolt_discord_msg = {} # maps revolt message id -> (discord channel id, discord message id)
discord_revolt_msg = {} # maps (discord channel id, discord message id) -> revolt message id

def new_link(revolt_id, discord_id):
    revolt_discord_msg[revolt_id] = discord_id
    discord_revolt_msg[discord_id] = revolt_id

def get_revolt_id(discord_id):
    if discord_id in discord_revolt_msg:
        return discord_revolt_msg[discord_id]
    else:
        return None

def get_discord_id(revolt_id):
    if revolt_id in revolt_discord_msg:
        return revolt_discord_msg[revolt_id]
    else:
        return None

def delete_discord_id(discord_id):
    del discord_revolt_msg[discord_id]

def delete_revolt_id(revolt_id):
    del revolt_discord_msg[revolt_id]