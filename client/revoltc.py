import defectio
import discord
from . import discordc
from . import msgmap

client = defectio.Client()

def discord_message_to_str(message):
    if message.author.nick is None:
        userstr = f"**{message.author.name}**#{message.author.discriminator}"
    else:
        userstr = f"**{message.author.nick}** ({message.author.name}#{message.author.discriminator})"
    
    attachments = ""
    for att in message.attachments:
        attachments += att.url + "\n"
    
    return f"{userstr} (discord)\n{message.content}\n{attachments}"

@client.event
async def on_ready():
    print("revolt client ready")

@client.event
async def on_message(message: defectio.Message):
    print("revolt message", message.content)

    if message.author.id != client.user.id:
        await discordc.revolt_message_to_discord(message)

@client.event
async def on_message_delete(message: defectio.Message):
    if message.author.id != client.user.id:
        await discordc.revolt_deleted((message.channel.id, message.id))

@client.event
async def on_message_edit(before: defectio.Message, message: defectio.Message):
    did = msgmap.get_discord_id((before.channel.id, before.id))
    msgmap.delete_revolt_id((before.channel.id, before.id))
    if did is not None:
        msgmap.delete_discord_id(did)
        msgmap.new_link((message.channel.id, message.id), did)

    if message.author.id != client.user.id:
        await discordc.revolt_edited(message)

async def discord_deleted(discord_id):
    revolt_id = msgmap.get_revolt_id(discord_id)
    if revolt_id is not None:
        chan = client.get_channel(revolt_id[0])
        await client.get_http().delete_message(revolt_id[0], revolt_id[1])

        msgmap.delete_revolt_id(revolt_id)

    msgmap.delete_discord_id(discord_id)

async def discord_edited(message):
    revolt_id = msgmap.get_revolt_id((message.channel.id, message.id))
    if revolt_id is not None:
        chan = client.get_channel(revolt_id[0])
        res = await client.get_http().edit_message(revolt_id[0], revolt_id[1], discord_message_to_str(message))

        msgmap.delete_revolt_id(revolt_id)

    msgmap.delete_discord_id((message.channel.id, message.id))


async def discord_message_to_revolt(message: discord.Message):
    dguild = message.guild.name
    dchannel = message.channel.name

    for server in client.servers:
        if server.name == dguild:
            for channel in server.channels:
                if channel.name == dchannel:
                    revolt_msg = await channel.send(discord_message_to_str(message))
                    msgmap.new_link((revolt_msg.channel.id, revolt_msg.id), (message.channel.id, message.id))
                    break
            break