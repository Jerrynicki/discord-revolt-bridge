import asyncio
import aiohttp
import json
import discord

import client

if __name__ == "__main__":
    config = json.load(open("config.json", "r"))

    loop = asyncio.get_event_loop()

    loop.create_task(client.revoltc.client.start(token=config["token_revolt"]))
    loop.create_task(client.discordc.client.start(config["token_discord"]))

    loop.run_forever()